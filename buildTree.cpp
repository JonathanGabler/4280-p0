//
// Created by JonGa on 9/22/2019.
//

#include "buildTree.h"
#include "node.h"
#include <iostream>

using namespace std;
// INSERT A NODE
TREE *insertNode (TREE *node, string word){
    // get the letter from the starting location
    char letter = word.at(0);
    try{
        if (!node) {
            node = createNode(word);
        }
            // Insert Middle: if left node is full, > than left but < than its node
        else if (node->left && letter < node->letter &&  node->left->letter < letter) {
            node->middle = insertNode(node->middle, word);
            node->middle->level = node->level + 1;
        }
            // Insert Left: when left when value less than its node's value and left is empty
        else if (letter < node->letter) {
            node->left = insertNode(node->left, word);
            node->left->level = node->level + 1;
        }
            // Insert Right: when value is greater than its nodes value
        else if (letter > node->letter) {
            node->right = insertNode(node->right, word);
            node->right->level = node->level + 1;
        }
        else if (letter == node->letter) {
            node->words.push_back(word);
        }
        return node;
    }
    catch(std::exception const& e){
        cout << "ERROR: An error occurred inserting nodes... " << e.what() << endl;
        return NULL;
    }
}

// BUILD THE TREE
TREE *buildTree(string filename) {
    //set the root of the tree
    TREE *root = NULL;
    char letter;
    string word;
    //Open the file
    ifstream file(filename.c_str());
    //Check if the file is open if not error out
    try{
        if(file.is_open()){
            cout << "File opened by buildTree.\n";
        }
    }
    catch(std::exception const& e){
        cout << "There was a file system error in buildTree.cpp: " << e.what() << endl;
        return NULL;
    }
    while (file >> word){
        for(int i = 0; i < word.size(); i++){
            //check for invalid chars. If found error out.
            if(!isalpha(word.at((unsigned int) i ))){
                cout << "ERROR: File contains invalid characters.";
                return NULL;
            }
        }
        // Assign the root value
        root = insertNode(root, word);
    }
    // Close the file
    file.close();
    return root;
}

// CREATE NODE
TREE *createNode (string word){
    TREE *temp = new TREE;
    temp->left = NULL;
    temp->right = NULL;
    temp->middle = NULL;
    temp->level = 0;
    temp->letter = word.at(0);
    temp->words.push_back(word);
    return temp;
}