//
// Created by JonGa on 9/22/2019.
//

#ifndef P0_NODE_H
#define P0_NODE_H


#include <vector>
#include <string>

using namespace std;
typedef struct node {
    int level;
    char letter;
    vector<string> words;
    node *left;
    node *right;
    node *middle;
} TREE;

#endif //P0_NODE_H
