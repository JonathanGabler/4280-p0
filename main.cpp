//
// Created by JonGa on 9/22/2019.
//
#include <iostream>
#include <fstream>
#include "node.h"
#include "buildTree.h"
#include "traversals.h"

using namespace std;

//Main
int main(int argc, char *argv[]){
    //Filename given
    string filename;
    //Input file
    string input;

    //CHECK FOR USER OPTION ARGUMENTS
    //Option 1: File is provided
    if(argc == 2){
        //set the filename to the one provided
        filename = argv[1];
        //append .input to eof name
        filename.append(".input");
    }
    // Option 2: No file is provided
    else if (argc == 1){
        ofstream tempFile;
        //Create a temp file
        filename = "tempData.input";
        //Open Temp file
        tempFile.open(filename.c_str());
        //Check to see if file was created and opened
        try{
            if(tempFile.is_open()){
                cout << "File created and opened successfully.\n";
            }
        }
        catch(std::exception const& e){
            cout << "There was a file system error: " << e.what() << endl;
        }
        //Print to console for user input
        printf("Input: No file was provided. \n Please input one character a-z until finished. Exit with ctrl + D.\n");
        //While user input print to temp file.
        while(cin >> input){
            tempFile << input << "\n";
        }
        tempFile.close(); //Close file
    }
    //Option 3: Non-valid arguments
    else{
        cout << "ERROR: Invalid input. Follow the format 'run [filename] \n";
        return 1;
    }

    TREE *root = buildTree(filename);
    cout << "\n Pre Order" << endl;
    preOrder(root);
    cout<<"\n In Order"<<endl;
    inOrder(root);
    cout<<"\n Post Order"<<endl;
    levelOrder(root);
}
