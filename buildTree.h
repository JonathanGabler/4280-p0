//
// Created by JonGa on 9/22/2019.
//

#ifndef P0_BUILDTREE_H
#define P0_BUILDTREE_H

#include "node.h"
#include <fstream>
#include <string.h>

using namespace std;

TREE *buildTree(string);
TREE *insertNode(TREE, string);
TREE *createNode(string);

#endif //P0_BUILDTREE_H
