//
// Created by JonGa on 9/22/2019.
//

#include "traversals.h"
#include <iostream>

using namespace std;

// IN ORDER TRAVERSAL
void inOrder(TREE *node){
    //Check if the root is null. If it is not then continue else break
    if(node == NULL){
        return;
    }
    else{
        inOrder(node -> left);
        inOrder(node -> middle);
        printSetup(node);
        inOrder(node -> right);
    }
}

// PRE ORDER TRAVERSAL
void preOrder(TREE *node){
    //check for node null. If not continue else break
    if(node == NULL){
        return;
    }
    else{
        printSetup(node);
        preOrder(node -> left);
        preOrder(node -> middle);
        preOrder(node ->right);
    }
}

// POST ORDER TRAVERSAL
void levelOrder(TREE *node){
    //check for node null. If not continue else break
    if(node == NULL){
        return;
    }
    else{
        levelOrder(node->left);
        levelOrder(node->middle);
        levelOrder(node->right);
        printSetup(node);
    }
}


// PRINT TREE SETUP
void printSetup(TREE *node){
    for(int i = 0; i < node -> level; i++){
        cout << " ";
    }
    cout << node -> letter << ":\t";
    for(int i = 0; i < (int) node -> words.size(); i++){
        cout << node -> words[i] << " ";
    }
    cout << "\n";
}