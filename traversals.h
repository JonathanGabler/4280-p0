//
// Created by JonGa on 9/22/2019.
//

#ifndef P0_TRAVERSALS_H
#define P0_TRAVERSALS_H

#include "node.h"

void preOrder(TREE *);
void inOrder(TREE *);
void levelOrder(TREE *);
void printSetup(TREE *);
void createFile(string, string);

#endif //P0_TRAVERSALS_H
